
#include <cstdlib>
#include <iostream>

#include <boost/scoped_ptr.hpp>


#include "ObjectsDetectionApplication.hpp"

#include <opencv2/opencv.hpp>

using namespace std;

// -~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~
int main(int argc, char *argv[])
{
    int ret = EXIT_SUCCESS;

    try
    {
        boost::scoped_ptr<doppia::ObjectsDetectionApplication>
                application_p( new doppia::ObjectsDetectionApplication() );

//        ret = application_p->main(argc, argv);

        ret = application_p->init(argc, argv);

        cv::Mat image = cv::imread("/home/dmitry/INRIAPerson/Test/pos/crop_000004.png");

        cv::imshow("", image);
        cv::waitKey();
        cv::destroyWindow("");

        std::vector<cv::Rect> detected_objects = application_p->compute(image);

        for(size_t i = 0; i < detected_objects.size(); ++i) {
            std::cout << detected_objects[i] << std::endl;
            cv::rectangle(image, detected_objects[i], cv::Scalar(0, 0, 255));
        }

        cv::imshow("", image);
        cv::waitKey();
        cv::destroyWindow("");
    }
    // on linux re-throw the exception in order to get the information
    catch (std::exception & e)
    {
        cout << "\033[1;31mA std::exception was raised:\033[0m " << e.what () << endl;
        ret = EXIT_FAILURE; // an error appeared
        throw;
    }
    catch (...)
    {
        cout << "\033[1;31mAn unknown exception was raised\033[0m " << endl;
        ret = EXIT_FAILURE; // an error appeared
        throw;
    }

    return ret;
}


